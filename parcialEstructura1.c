#include <stdio.h>
#define tam 3
int main(void){
	//Creando las matrices
	int matrizA[tam][tam];
	int matrizB[tam][tam];
	//Llenado de las matrices
	int fila, col, burbuja, valor, fila2, col2;
	//LLENAR Matriz A
	printf("Vamos a llenar la matriz A \n");
	for(fila=0; fila<tam; fila++){
		for(col=0; col<tam; col++){
			printf("Ingrese un número \n");
			scanf("%i", &valor);
			matrizA[fila][col]=valor;
		}
	}
	//LLENAR Matriz B
	printf("Vamos a llenar la matriz B \n");
	for(fila=0; fila<tam; fila++){
		for(col=0; col<tam; col++){
			printf("Ingrese un número \n");
			scanf("%i", &valor);
			matrizB[fila][col]=valor;
		}
	}
	//Ordenando las matrices
	//ORDENAR Matriz A
	for(fila=0; fila<tam; fila++){
		for(col=0; col<tam; col++){
			for(fila2=0; fila2<tam; fila2++){
				for(col2=0; col2<tam; col2++){
					if(matrizA[fila][col]<matrizA[fila2][col2]){
						burbuja=matrizA[fila][col];
						matrizA[fila][col]=matrizA[fila2][col2];
						matrizA[fila2][col2]=matrizA[fila][col];
					}					
				}
			}
		}
	}
	//Imprimir matrizA ordenada
	for(fila=0; fila<tam; fila++){
		printf("\n");
		for(col=0; col<tam; col++){
			printf("%d \t", matrizA[fila][col]);
			
		}
	}

	// - - -Si obviamos la burbuja :$ - - -
	//Imprimir matrizA ordenada
	printf("Matriz A");
	for(fila=0; fila<tam; fila++){
		printf("\n");
		for(col=0; col<tam; col++){
			printf("%d \t", matrizA[fila][col]);
			
		}
	}
	//Imprimir matrizB ordenada
	printf("\nMatriz B");
	for(fila=0; fila<tam; fila++){
		printf("\n");
		for(col=0; col<tam; col++){
			printf("%d \t", matrizB[fila][col]);
			
		}
	}
	//Le pongo tamaño 18 al vector, porque las matrices son de 3x3, por lo tanto tendrá 9 datos cada una, entre las dos son 18.
	int vectorOrdenado[18];
		int pos=0;
		for(fila=0; fila<tam; fila++){
			for(col=0; col<tam; col++){
				for(fila2=0; fila2<tam; fila2++){
					for(col2=0; col2<tam; col++){
						if(matrizA[fila][col]<matrizB[fila2][col2]){
							vectorOrdenado[pos]=matrizA[fila][col];
						}
						else{
							vectorOrdenado[pos]=matrizB[fila2][col2];
						}
						pos=pos++;
						
					}
				}
			}
		}
		for(pos=0; pos<18; pos++){
			printf("%d", vectorOrdenado[pos]);
		}
}
